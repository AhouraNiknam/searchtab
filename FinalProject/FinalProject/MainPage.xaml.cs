﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FinalProject
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        async void Login_Clicked(object sender, System.EventArgs e)
        {
            if(Username_Entry.Text != null || Password_Entry.Text != null)
            {
                await DisplayAlert("Just so you know", "No need for Account Info at this time", "OK");
                await Navigation.PushAsync(new HomePage(Username_Entry.Text));
            }
            else
                await Navigation.PushAsync(new HomePage(Username_Entry.Text));
        }

        async void NewAccount_Clicked(object sender, System.EventArgs e)
        {
            await DisplayAlert("Just so you know", "No need for Account Info at this time", "OK");
            await Navigation.PushAsync(new NewAccountPage()); //go to the new account creation page
        }

        async void ForgotPassword_Clicked(object sender, System.EventArgs e)
        {
            await DisplayAlert("Just so you know", "This Button Does Nothing", "OK");
            return;
        }
    }
}
