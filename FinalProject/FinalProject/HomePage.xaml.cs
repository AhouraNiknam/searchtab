﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
            //NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();
		}

        public HomePage (String username)
        {
            //NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            if (username == null || username == "")
            {
                username = "placeholder";
            }
            Username.Text = username + "!";
        }

        async void Enter_App(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AppContentPage());
        }

        private async void Logout_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }
    }
}